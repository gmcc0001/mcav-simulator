# MCAV-Simulator

Unity version: 2019.3.7f1

Using Unity HDRP

Anything imported from older versions of unity must have their materials upgraded: Edit > Render Pipeline > Upgrade Project (or Selected) Materials to High Definition Materials


External Packages:

ROS#:
https://assetstore.unity.com/packages/tools/physics/ros-107085

Small Town America - Streets:
https://assetstore.unity.com/top-assets/top-free?q=roads&orderBy=4&page=2
