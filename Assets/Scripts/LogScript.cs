﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Normal Log");
        Debug.LogWarning("Warning");
        Debug.LogError("Error");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
