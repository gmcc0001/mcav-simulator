﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RosSharp.RosBridgeClient;

public class ReceiveTranslateRotate : MonoBehaviour {
    public GameObject ROS_data;
    private Transform ROS_data_t;
    private Vector3 pos;
    private Vector3 rot;
    public float y_rot_offset;
    private Rigidbody rb;
    RaycastHit hit;
    private Vector3 old_pos;
    public bool zero_flag = false;
    private OdometrySubscriber ros_script;

    public float angular;

    
    void Start () {
        // The car (odom) object which is flying above the car
        ROS_data_t = ROS_data.GetComponent<Transform>();
        rb = this.GetComponent<Rigidbody>();
        ros_script = ROS_data.GetComponent<OdometrySubscriber>();
    }
    

    void Update()
    {
        if (!zero_flag) // if the car is off the ground
        {
            if (Mathf.Abs(ROS_data_t.position.x) > 1)
            {
                //Debug.Log(ROS_data_t.position);
                //Debug.Log("Hello?");
                // position given from ros
                Vector3 temp = ROS_data_t.position;
                // minus 2 off the y value
                temp[1] = temp[1] - 2;
                // send a raycast down
                if (Physics.Raycast(temp, Vector3.down, out hit, Mathf.Infinity))
                {
                    pos.x = temp.x;
                    pos.y = temp.y - hit.distance + 0.5f; // transform the car to the ground (from the raycast)
                    pos.z = temp.z;
                    this.transform.position = pos;

                    old_pos = temp;
                    //Debug.Log(hit.distance);
                    //Debug.LogWarning(hit.collider);
                    zero_flag = true;
                }
                Debug.Log("Bye");
            }
        }

        rb.AddForce(0, -10, 0, ForceMode.Acceleration);
        Debug.DrawRay(old_pos, Vector3.down * hit.distance, Color.magenta);
        //Debug.DrawRay(ROS_data_t.position, Vector3.down * 1000, Color.red);
    }

    void LateUpdate () {

        if (zero_flag)
        {
            // take x and z from car (odom)
            pos.x = ROS_data_t.position.x;
            pos.y = this.transform.position.y; // y is taken from the cars current position
            pos.z = ROS_data_t.position.z;
            this.transform.position = pos;

            // x and z rotation taken from current angles
            rot.x = this.transform.eulerAngles.x;
            rot.y = ROS_data_t.eulerAngles.y + y_rot_offset; // y is taken from the car (odom)
            rot.z = this.transform.eulerAngles.z;

            angular = (rot.y - transform.eulerAngles.y)/Time.deltaTime;

            this.transform.eulerAngles = rot;

        }
    }
}
