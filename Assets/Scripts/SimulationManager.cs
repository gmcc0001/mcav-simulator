﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationManager : MonoBehaviour
{
    // stores the car objects
    public GameObject [] cars;
    // stores the controller scripts - enables movement of car
    private SimpleCarController[] carControllers;
    // index that tracks the current car being used
    private int currentCarSelected;

    // reference to the camera follow script 
    public CameraFollowController cameraFollow;
    // the key that is used to swap cars
    public KeyCode swapCarControlKey;

    void Start()
    {
        // initialise array with the correct length
        carControllers = new SimpleCarController[cars.Length];

        // set all of the controllers to disabled
        for(int i=0; i < cars.Length; i++)
        {
            carControllers[i] = cars[i].GetComponent<SimpleCarController>();
            carControllers[i].enabled = false;
        }
        cameraFollow.objectToFollow = cars[currentCarSelected].transform;
        // enable the initial car
        carControllers[0].enabled = true;
    }
    void Update()
    {
        // if the key is pressed
        if (Input.GetKeyDown(swapCarControlKey))
        {
            //  run the function
            swapCarControl();
        }
    }

    void swapCarControl()
    {
        // disable control of car
        carControllers[currentCarSelected].enabled = false;
        // increment the car index
        currentCarSelected = (currentCarSelected+1)%cars.Length;
        // enable the new car
        carControllers[currentCarSelected].enabled = true;
        // set the camera to follow it
        cameraFollow.objectToFollow = cars[currentCarSelected].transform;
    }
}
